package Exceptiones;

import java.io.File;

public class CarpetaNoExiste extends Exception {
    private File carpeta;

    public CarpetaNoExiste(File carpeta) {
        this.carpeta = carpeta;
    }

    @Override
    public String toString() {
        return "La carpeta " + carpeta.getName() + " no existe.";
    }
}
