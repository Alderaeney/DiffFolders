package Exceptiones;

import java.io.File;

public class GestioFitxersException extends Exception {

    private File archivo;

    public GestioFitxersException(File archivo) {
        this.archivo = archivo;
    }

    @Override
    public String toString() {
        return archivo.getName() + " no es un directori";
    }
}
