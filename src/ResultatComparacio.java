public class ResultatComparacio {

    private String nom;
    private ValorComparacio valor;

    public ResultatComparacio(String nom, ValorComparacio valor) {
        this.nom = nom;
        this.valor = valor;
    }

    @Override
    public String toString() {
        return nom + "\t" + valor;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public ValorComparacio getValor() {
        return valor;
    }

    public void setValor(ValorComparacio valor) {
        this.valor = valor;
    }
}
