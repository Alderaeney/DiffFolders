import Exceptiones.CarpetaNoExiste;
import Exceptiones.GestioFitxersException;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class DiffFolders {

    private File folder1;
    private File folder2;

    public DiffFolders() {
    }

    public void setFolders(File folder1, File folder2) throws GestioFitxersException, CarpetaNoExiste {
        if (folder1.exists() && folder2.exists()){
            if (folder1.isDirectory() && folder2.isDirectory()){
                this.setFolder1(folder1);
                this.setFolder2(folder2);
            } else {
                if (!folder1.isDirectory()){
                    throw new GestioFitxersException(folder1);
                } else throw new GestioFitxersException(folder2);
            }
        } else {
            if (!folder1.exists()){
                throw new CarpetaNoExiste(folder1);
            } else {
                throw new CarpetaNoExiste(folder2);
            }
        }
    }

    public Iterator<ResultatComparacio> compare(){
        ResultatComparacio result;
        ArrayList<ResultatComparacio> llista = new ArrayList<>();
        File[] fitxers1 = folder1.listFiles();
        File[] fitxers2 = folder2.listFiles();

        for (File fitx1 :
                fitxers1) {
            int cont = 0;
            boolean encontrado = false;
            for (File fitx2 :
                    fitxers2) {
                if (fitx1.getName().equals(fitx2.getName())) {
                    if (fitx1.length() == fitx2.length() && fitx1.lastModified() == fitx2.lastModified()){
                        result = new ResultatComparacio(fitx1.getName(), ValorComparacio.IGUALS);
                        llista.add(result);
                    } else if (fitx1.lastModified() > fitx2.lastModified()){
                        result = new ResultatComparacio(fitx1.getName(), ValorComparacio.MES_NOU_EN_1);
                        llista.add(result);
                    } else {
                        result = new ResultatComparacio(fitx2.getName(), ValorComparacio.MES_NOU_EN_2);
                        llista.add(result);
                    }
                    encontrado = true;
                } else if (cont == fitxers2.length - 1 && !encontrado){
                    result = new ResultatComparacio(fitx1.getName(), ValorComparacio.FALTA_EN_2);
                    llista.add(result);
                }
                cont++;
            }
        }

        for (File fitx2 :
                fitxers2) {
            boolean trobat = false;
            for (ResultatComparacio resultat :
                    llista) {
                if (fitx2.getName().equals(resultat.getNom())) {
                    trobat = true;
                }
            }
            if (!trobat){
                result = new ResultatComparacio(fitx2.getName(), ValorComparacio.FALTA_EN_1);
                llista.add(result);
            }
        }

        return llista.iterator();
    }

    @Override
    public String toString() {
        return "folder1=" + folder1.getName() +
                ", folder2=" + folder2.getName() +
                '}';
    }

    public File getFolder1() {
        return folder1;
    }

    public void setFolder1(File folder1) {
        this.folder1 = folder1;
    }

    public File getFolder2() {
        return folder2;
    }

    public void setFolder2(File folder2) {
        this.folder2 = folder2;
    }
}
