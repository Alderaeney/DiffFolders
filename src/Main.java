import Exceptiones.CarpetaNoExiste;
import Exceptiones.GestioFitxersException;

import java.io.File;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        DiffFolders diffFolders = new DiffFolders();
        String ruta1, ruta2;

        System.out.print("Introduce ruta a la carpeta1: ");
        ruta1 = scanner.nextLine();

        System.out.print("Introduce ruta a la carpeta2: ");
        ruta2 = scanner.nextLine();


        try{
            diffFolders.setFolders(new File(ruta1), new File(ruta2));
            Iterator<ResultatComparacio> it = diffFolders.compare();

            System.out.println("\t NOMBRE FICHERO" + "\t VALOR RESULTADO");
            while (it.hasNext()){
                System.out.println(it.next().toString());
            }
        } catch (GestioFitxersException ge){
            System.out.println(ge.toString());
        } catch (CarpetaNoExiste cp){
            System.out.println(cp.toString());
        }



    }
}
